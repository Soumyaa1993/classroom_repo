package com.testng.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Method_code {
	
		static WebDriver driver;
	    public void browserMercuryAppLaunch() throws InterruptedException {

	                    System.setProperty("webdriver.chrome.driver", "./chrom_driver/chromedriver.exe");

	                    driver = new ChromeDriver();
	                    //log.info("Browser has been launched");
	                    Thread.sleep(3000); //paused 3s
	                    driver.manage().window().maximize(); 
	                    Thread.sleep(3000);
	                    driver.get("http://newtours.demoaut.com/");
	                    Thread.sleep(3000);
	    }
	    
	    public void login (String username,String Password) throws InterruptedException
	    {
	    	WebElement uname = driver.findElement(By.xpath("//input[@name='userName']"));
	    	uname.sendKeys(username);
	    	WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
	    	pwd.sendKeys(Password);
	    	WebElement login = driver.findElement(By.xpath("//input[@value='Login']"));
			login.click();
			Thread.sleep(4000);
	    	
	    }
	    public boolean validation () {
	    	String expTitle ="Find a Flight: Mercury Tours:";
	    	String actTitle = driver.getTitle();
	    			if(expTitle.equals(actTitle))
	    			{
	    				return true;
	    		
	    			}else {
					return false;
	    	
	    }
	    
	    }
	    public boolean INvalidation () {
	    	String expTitle ="Find a Flight: Mercury Tours:";
	    	String actTitle = driver.getTitle();
	    			if(expTitle!=(actTitle))
	    			{
	    				return true;
	    		
	    			}else {
					return false;
	    	
	    }
	    }
	    public boolean verifyingroundTrip()
	    {
	    	WebElement radioBtnOneWay = driver.findElement(By.xpath("//input[@value='oneway']"));
			WebElement radioBtnRoundTrip = driver.findElement(By.xpath("//input[@value='roundtrip']"));
			if (radioBtnRoundTrip.isSelected()== true && radioBtnOneWay.isSelected()== false)
			{
				return true;
			}
			return false;
	    	
	    }
	    public boolean selection() throws InterruptedException
	    {
	    	WebElement destination = driver.findElement(By.xpath("select[@name='fromMonth']"));
	    	Select s1 = new Select(destination);
	    	Thread.sleep(3000);
	    	s1.selectByVisibleText("London");
	    	Thread.sleep(4000);
	    	if(destination.getText().equals("London"))
	    	{
	    		return true;
	    	}
	    	else 
	    	{
	    		return false;
	    	
	    }
	    }

		public void appClose()
		{
			driver.close();
		}
	    
}


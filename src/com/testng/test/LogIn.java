package com.testng.test;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LogIn {
	
	WebDriver driver;
	Method_code tst= new Method_code();
	
	@BeforeMethod
	public void applunge() throws InterruptedException
	{
		tst.browserMercuryAppLaunch();
		
	}
	/*
	 * tc 001 login check
	 */
	@Test(priority = 0, enabled=true, description = " 001 login check")
	public void login()
	{
		try {
			tst.login("dasd", "dasd");
			Assert.assertEquals(true, tst.validation());
		}
		catch(Exception outputt)
		{
			System.out.println(outputt);
		}
		
	}
	@Test(priority = 1, enabled=true, description="TC_002: Verifying departing from desired value selection.")
	public void verifyDepartingFromValueSelection() {
		try {
			tst.login("dasd", "dasd");
			Assert.assertEquals(true, tst.INvalidation());
		}catch(Exception e){
			System.out.println(e);
		}
	}
	
	@AfterMethod
	public void closeApp() 
	{
		tst.appClose();
	}

}

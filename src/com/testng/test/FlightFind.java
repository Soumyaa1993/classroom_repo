package com.testng.test;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FlightFind {
	WebDriver driver;
	Method_code tst= new Method_code();
	
	@BeforeMethod
	public void applunge() throws InterruptedException
	{
		tst.browserMercuryAppLaunch();
		
	}
	/*
	 * tc 001 verifyingroundTrip
	 */
	@Test(priority = 0, enabled=true, description = " 001 verifyingroundTrip")
	public void verifyingroundTrip()
	{
		try {
			tst.login("dasd", "dasd");
			Assert.assertEquals(true, tst.verifyingroundTrip());
		}
		catch(Exception outputt)
		{
			System.out.println(outputt);
		}
	}
	/*
	 * tc 002 destination selection
	 */
	@Test(priority=1, enabled=true, description= "tc 002 destination selection")
	public void destination_selection()
	{
		try {
			tst.login("dasd", "dasd");
			Assert.assertEquals(true, tst.selection());
		}
		catch(Exception outputt)
		{
			System.out.println(outputt);
		}
	}
		@AfterMethod
		public void closeApp() {
			tst.appClose();
		}
	
	 

}
